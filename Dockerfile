#FROM gradle:5.2.1-jdk8-alpine AS build
#COPY --chown=gradle:gradle . /home/gradle/src
#WORKDIR /home/gradle/src

FROM openjdk:8

#RUN mkdir -p /root/.gradle
#ENV HOME /root
#VOLUME /root/.gradle

ENTRYPOINT ["/bin/bash"]
