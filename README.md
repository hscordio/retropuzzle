
----------------------------------  
A-Maze-ingly Retro Route Puzzle  
----------------------------------  
  
Problem:  
Write a program that will output a valid route one could follow to collect all specified items within a map. The map is a json description of set of  
rooms with allowed path and contained object.  
  
Exercize starts with an input of:  
  
1. json reppresentation of map  
  
2. starting room  
  
3. list of object to collect  
  
Room type allowed fields  
  
id: Integer  
  
name: String  
  
north: Integer //referring to a connected room  
  
south: Integer //referring to a connected room  
  
west: Integer //referring to a connected room  
  
east: Integer //referring to a connected room  
  
objects: List //of Objects  
  
Object type allowed fields  
  
name: String //Object Name  
  
----------------------------------  
Example 1:  
----------------------------------  
  
Map  
  
{  
  
"rooms": [  
  
{ "id": 1, "name": "Hallway", "north": 2, "objects": [] },  
  
{ "id": 2, "name": "Dining Room", "south": 1, "west": 3, "east": 4, "objects": [] },  
  
{ "id": 3, "name": "Kitchen","east":2, "objects": [ { "name": "Knife" } ] },  
  
{ "id": 4, "name": "Sun Room","west":2, "objects": [ { "name": "Potted  
Plant" } ] }  
  
]  
  
}  
  
Input Start Room ID= 2  
  
Input Objects To Collect= Knife, Potted Plant  
  
----------------------------------  
Output  
----------------------------------  
  
ID Room Object collected  
  
2 Dining Room None  
  
1 Hallway None  
  
2 Dining Room None  
  
3 Kitchen Knife  
  
2 Dining Room None  
  
4 Sun Room Potted Plant  
  
----------------------------------  
Example 2  
----------------------------------  
  
Map  
  
{  
  
"rooms": [  
  
{ "id": 1, "name": "Hallway", "north": 2, "east":7, "objects": [] },  
  
{ "id": 2, "name": "Dining Room", "north": 5, "south": 1, "west": 3, "east": 4, "objects": [] },  
  
{ "id": 3, "name": "Kitchen","east":2, "objects": [ { "name": "Knife" } ] },  
  
{ "id": 4, "name": "Sun Room","west":2, "north":6, "south":7, "objects": [] },  
  
{ "id": 5, "name": "Bedroom","south":2, "east":6, "objects": [{ "name": "Pillow" }] },  
  
{ "id": 6, "name": "Bathroom","west":5, "south":4, "objects": [] },  
  
{ "id": 7, "name": "Living room","west":1, "north":4, "objects": [{"name": "Potted Plant" }] }  
  
]  
  
}  
  
Input Start Room ID = 4  
  
Input Objects To Collect= Knife, Potted Plant, Pillow  
  
----------------------------------  
Output  
----------------------------------  
  
ID Room Object collected  
  
4 Sun Room None  
  
6 Bathroom None  
  
4 Sun Room None  
  
7 Living room Potted Plant  
  
4 Sun Room None  
  
2 Dining Room None  
  
5 Bedroom Pillow  
  
2 Dining Room None  
  
1 Hallway None  
  
2 Dining Room None  
  
3 Kitchen Knife  
  
Goals:  
  
TDD approach.  
  
Build a Docker container with runnable code inside so that we can mount a volume in it and test on different maps.  
  
----------------  
INSTALLATION & USAGE  
----------------  
  
**Requirements** 
  
1. Git Console (i.e. Git Bash)  
  
2. Docker  
  
**Installation Steps**  
  
1. `git clone https://hscordio@bitbucket.org/hscordio/retropuzzle.git`  
  
2. `cd retropuzzle`  
  
3. `docker build -t retropuzzle .`  
  
**Usage**  

    docker run -v $(pwd):/mnt -w /mnt retropuzzle ./scripts/build.sh
    docker run -v $(pwd):/mnt -w /mnt retropuzzle ./scripts/tests.sh
    docker run -v $(pwd):/mnt -w /mnt retropuzzle ./scripts/run.sh

**Notes about configurations** 
  
Folder `src/main/resources/maps` contains few json files each one representing a maze map.  
  
Folder `src/main/resources/parameters` contains four files:  
  
`parameters-test1.properties`: configurations for the Example 1 
  
`parameters-test2.properties`: configurations for the Example 2
  
`parameters-test3.properties` and `parameters-test4.properties`: test the algorithm on different situations
  
A first way to test the algorithm on other situations is to load further json maps under `src/main/resources/maps` and the properties file under `src/main/resources/parameters`.  
  
The second solution is to load parameters (and the maps accordingly) in any folder within the project (i.e. data folder).

In order to run tests with the first solution the `scripts/run.sh` should be setup in this way:

    ./gradlew exec --args='/parameters/parameters-test1.properties'
    ./gradlew exec --args='/parameters/parameters-test2.properties'
    ./gradlew exec --args='/parameters/parameters-test3.properties'
    ./gradlew exec --args='/parameters/parameters-test4.properties'

To run the tests with the second solution:

    ./gradlew exec --args='e ./data/parameters/parameters-test1.properties'
    ./gradlew exec --args='e ./data/parameters/parameters-test2.properties' 
    ./gradlew exec --args='e ./data/parameters/parameters-test3.properties'
    ./gradlew exec --args='e ./data/parameters/parameters-test4.properties'  

The `scripts/run.sh` file is already prepared to run with the second solution.
