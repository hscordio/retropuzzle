# If the parameter file is in the classpath i.e. resources folder
# ./gradlew exec --args='/parameters/parameters-test.properties'
# If the parameter file is not in the classpath i.e: ./data folder
# ./gradlew exec --args='e ./data/parameters/parameters-test.properties'
./gradlew exec --args='e ./data/parameters/parameters-test1.properties'
./gradlew exec --args='e ./data/parameters/parameters-test2.properties'
./gradlew exec --args='e ./data/parameters/parameters-test3.properties'
./gradlew exec --args='e ./data/parameters/parameters-test4.properties'
