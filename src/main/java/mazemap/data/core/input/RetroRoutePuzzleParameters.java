package mazemap.data.core.input;

import com.google.common.base.Objects;

public class RetroRoutePuzzleParameters {

    private final FileInfo mazeFile;
    private final String startRoomId;
    private final String itemsToCollect;

    RetroRoutePuzzleParameters(final FileInfo mazeFile,
                               final String startRoomId,
                               final String itemsToCollect) {
        this.mazeFile = mazeFile;
        this.startRoomId = startRoomId;
        this.itemsToCollect = itemsToCollect;
    }

    public FileInfo getMazeFile() {
        return mazeFile;
    }

    public String getStartRoomId() {
        return startRoomId;
    }

    public String getItemsToCollect() {
        return itemsToCollect;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final RetroRoutePuzzleParameters that = (RetroRoutePuzzleParameters) o;
        return Objects.equal(mazeFile, that.mazeFile) &&
                Objects.equal(startRoomId, that.startRoomId) &&
                Objects.equal(itemsToCollect, that.itemsToCollect);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mazeFile, startRoomId, itemsToCollect);
    }
}
