package mazemap.data.core.input;

import com.google.common.base.Objects;
import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import java.util.Set;

public class RetroRoutePuzzleInput {

    private final MazeMap mazeMap;
    private final Integer startRoomId;
	private final Set<Item> itemsToCollect;
    private final String mazeFilePath;

    RetroRoutePuzzleInput(final MazeMap mazeMap,
                          final Integer startRoomId,
                          final Set<Item> itemsToCollect,
                          final String mazeFilePath) {
        this.mazeMap = mazeMap;
        this.startRoomId = startRoomId;
        this.itemsToCollect = itemsToCollect;
        this.mazeFilePath = mazeFilePath;
    }

    public MazeMap getMazeMap() {
        return mazeMap;
    }

    public Integer getStartRoomId() {
        return startRoomId;
    }

    public Set<Item> getItemsToCollect() {
        return itemsToCollect;
    }

    @Override
    public String toString() {
        final StringBuilder string = new StringBuilder();
        final String newLine = System.getProperty("line.separator");
        string.append("--------- INPUT ------------").append(newLine);
        string.append("MazeMap file path: ").append(mazeFilePath).append(newLine);
        string.append("---------- MAZE ------------").append(newLine);
        string.append(mazeMap).append(newLine);
        string.append("------ STARTING ROOM -------").append(newLine);
        string.append(startRoomId).append(newLine);
        string.append("---------- ITEMS ------------").append(newLine);
        string.append(itemsToCollect);
        return string.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final RetroRoutePuzzleInput that = (RetroRoutePuzzleInput) o;
        return Objects.equal(mazeMap, that.mazeMap) &&
                Objects.equal(startRoomId, that.startRoomId) &&
                Objects.equal(itemsToCollect, that.itemsToCollect) &&
                Objects.equal(mazeFilePath, that.mazeFilePath);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mazeMap, startRoomId, itemsToCollect, mazeFilePath);
    }
}
