package mazemap.data.core.input;

public class ArgumentParser {

    private static final String DEFAULT_PARAMETERS_FILE_PATH = "/parameters/parameters-test.properties";

    public static FileInfo getFileInfo(final String[] args) {
        if (args == null) {
            return new FileInfo(false, DEFAULT_PARAMETERS_FILE_PATH);
        }
        final boolean defaultFilePath = args.length == 0;
        final boolean internalFilePath = args.length == 1;
        final String filePath = defaultFilePath ? DEFAULT_PARAMETERS_FILE_PATH
                : internalFilePath ? args[0]
                : args[1];
        return new FileInfo(args.length == 2, filePath);
    }
}
