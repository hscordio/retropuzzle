package mazemap.data.core.input;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

class FileRetriever {

    static InputStream retrieveFile(final FileInfo info) throws FileNotFoundException {
        return info.getExternal() ? new FileInputStream(info.getPath())
                : FileRetriever.class.getResourceAsStream(info.getPath());
    }
}
