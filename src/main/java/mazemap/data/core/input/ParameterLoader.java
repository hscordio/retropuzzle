package mazemap.data.core.input;

import java.io.IOException;
import java.util.Properties;

public class ParameterLoader {

    private final FileInfo fileInfo;

    public ParameterLoader(final FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

    public RetroRoutePuzzleParameters load() throws IOException {
        final Properties properties = PropertyLoader.loadPropertiesFromFile(fileInfo);
        final FileInfo mazeFile = new FileInfo(fileInfo.getExternal(), properties.getProperty("maze.file"));
        final String startRoomId = properties.getProperty("maze.start.room");
        final String itemsToCollect = properties.getProperty("maze.items.collect");
        return new RetroRoutePuzzleParameters(mazeFile, startRoomId, itemsToCollect);
    }
}
