package mazemap.data.core.input;
import com.fasterxml.jackson.databind.ObjectMapper;
import mazemap.data.core.MazeMap;

import static mazemap.data.core.input.FileRetriever.retrieveFile;

class MazeMapFactory {

    static MazeMap buildMaze(final FileInfo fileInfo) throws Exception {
        final MazeMap mazeMap = new ObjectMapper().readValue(retrieveFile(fileInfo), MazeMap.class);;
        mazeMap.initializeNeighbours();
        return mazeMap;
    }
}
