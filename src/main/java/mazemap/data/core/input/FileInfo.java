package mazemap.data.core.input;

import com.google.common.base.Objects;

public class FileInfo {

    private final boolean external;
    private final String path;

    public FileInfo(final boolean external, final String path) {
        this.external = external;
        this.path = path;
    }

    boolean getExternal() {
        return external;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final FileInfo fileInfo = (FileInfo) o;
        return external == fileInfo.external &&
                Objects.equal(path, fileInfo.path);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(external, path);
    }
}
