package mazemap.data.core.input;

import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;

import java.util.Set;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

public class RetroRoutePuzzleInputBuilder {

    private final RetroRoutePuzzleParameters parameters;
    private static final String ITEMS_SEPARATOR = ",";

    public RetroRoutePuzzleInputBuilder(final RetroRoutePuzzleParameters parameters) {
        this.parameters = parameters;
    }

    public RetroRoutePuzzleInput buildRetroRoutePuzzleInput() throws Exception {
        final MazeMap mazeMap = MazeMapFactory.buildMaze(parameters.getMazeFile());
        final Set<Item> itemsToCollect = stream(parameters.getItemsToCollect().split(ITEMS_SEPARATOR))
                .map(String::trim)
                .map(Item::new)
                .collect(toSet());
        return new RetroRoutePuzzleInput(mazeMap,
                parseInt(parameters.getStartRoomId()),
                itemsToCollect,
                parameters.getMazeFile().getPath());
    }
}
