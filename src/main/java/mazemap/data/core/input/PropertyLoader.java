package mazemap.data.core.input;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static mazemap.data.core.input.FileRetriever.retrieveFile;

class PropertyLoader {

    static Properties loadPropertiesFromFile(final FileInfo fileInfo) throws IOException {
        final InputStream propsInputStreams = retrieveFile(fileInfo);
        final Properties prop = new Properties();
        prop.load(propsInputStreams);
        propsInputStreams.close();
        return prop;
    }
}
