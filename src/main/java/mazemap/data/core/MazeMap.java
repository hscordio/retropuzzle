package mazemap.data.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class MazeMap {

    private final List<Room> rooms;
    final private Map<Integer, Room> roomById;

    public MazeMap(@JsonProperty("rooms") final List<Room> rooms) {
        this.rooms = rooms;
        this.roomById = rooms.stream().collect(toMap(Room::getId, identity()));
    }

    @JsonCreator
    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    public String toString() {
        final String newLine = System.getProperty("line.separator");
        final StringBuffer roomsString = new StringBuffer();
        roomsString.append("MazeMap {").append(newLine);
        rooms.forEach(room -> roomsString.append(room.toString()).append(newLine));
        roomsString.append("}");
        return roomsString.toString();
    }

    public Room findRoomById(final Integer id) {
        return roomById.get(id);
    }

    public void initializeNeighbours() {
        getRooms().forEach(room ->
                room.setNeighbours(
                        Stream.of(room.getNorth(), room.getEast(), room.getSouth(), room.getWest())
                                .filter(Objects::nonNull)
                                .map(this::findRoomById)
                                .collect(Collectors.toList()))
        );
    }
}
