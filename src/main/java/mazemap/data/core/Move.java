package mazemap.data.core;

import com.google.common.base.Objects;

public class Move {

	private final Integer source;
	private final Integer destination;
	
	public Move(final Integer source, final Integer destination) {
		this.source = source;
		this.destination = destination;
	}
	
	public Integer getSource() {
		return source;
	}
	public Integer getDestination() {
		return destination;
	}

	@Override
	public String toString() {
		return "["+source + ","+ destination+"]";
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final Move move = (Move) o;
		return Objects.equal(source, move.source) &&
				Objects.equal(destination, move.destination);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(source, destination);
	}
}
