package mazemap.data.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Item {

    private final String name;

    @JsonCreator
    public Item(@JsonProperty("name") final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final Item item = (Item) o;
		return com.google.common.base.Objects.equal(name, item.name);
	}

	@Override
	public int hashCode() {
		return com.google.common.base.Objects.hashCode(name);
	}
}
