package mazemap.data.core;


import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;


public class Room {

    private final Integer id;
    private final String name;
    private final Integer north;
    private final Integer south;
    private final Integer east;
    private final Integer west;
    private final Set<Item> items;
    private List<Room> neighbours = null;

    @JsonCreator
    public Room(@JsonProperty("id") final Integer id,
                @JsonProperty("name") final String name,
                @JsonProperty("north") final Integer north,
                @JsonProperty("south") final Integer south,
                @JsonProperty("east") final Integer east,
                @JsonProperty("west") final Integer west,
                @JsonProperty("objects") final Set<Item> items) {
        this.id = id;
        this.name = name;
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
        this.items = items;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getNorth() {
        return north;
    }

    public Integer getSouth() {
        return south;
    }

    public Integer getEast() {
        return east;
    }

    public Integer getWest() {
        return west;
    }

    public Set<Item> getItems() {
        return items;
    }

    public List<Room> getNeighbours() {
        return neighbours;
    }

    void setNeighbours(final List<Room> neighbours) {
        this.neighbours = neighbours;
    }

    @Override
    public String toString() {
        final StringBuilder roomString = new StringBuilder();
        roomString.append("Room {id=").append(getId()).append(", name=").append(getName()).append(", neighbours=");
        roomString.append("[").append(neighbours.stream().map(e -> "" + e.getId()).collect(Collectors.joining(","))).append("], items=");
        roomString.append(getItems().size() == 0 ? "None" : getItems()).append("}");
        return roomString.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Room room = (Room) o;
        return Objects.equal(id, room.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
