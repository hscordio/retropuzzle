package mazemap.routing.output;

import java.util.List;
import java.util.Set;

import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import mazemap.data.core.Move;
import mazemap.data.core.Room;

public class RetroRoutePuzzleOutput {

    private final RetroRoutePuzzleResult result;
    private final MazeMap mazeMap;

    public RetroRoutePuzzleOutput(final RetroRoutePuzzleResult result,
                                  final MazeMap mazeMap) {
        this.result = result;
        this.mazeMap = mazeMap;
    }

    @Override
    public String toString() {
        final StringBuilder output = new StringBuilder();
        final String newLine = System.getProperty("line.separator");
        output.append("---------- OUTPUT ----------").append(newLine);
        output.append("---------").append(result.getStatus().toString()).append("----------").append(newLine);
        final List<Move> route = result.getRoute();
        Room lastDestination = null;
        for (final Move move : route) {
            final Room sourceRoom = mazeMap.findRoomById(move.getSource());
            final Room destinationRoom = mazeMap.findRoomById(move.getDestination());
            if (sourceRoom != null && !sourceRoom.equals(lastDestination)) {
                appendRoomOutputInfo(output, newLine, sourceRoom);
            }
            appendRoomOutputInfo(output, newLine, destinationRoom);
            lastDestination = destinationRoom;
        }
        final Set<Item> itemsNotFound = result.getItemsNotFound();
        if (!itemsNotFound.isEmpty()) {
            output.append("Items Not Found: ").append(itemsNotFound).append(newLine);
        }
        return output.toString();
    }

    private void appendRoomOutputInfo(final StringBuilder output, final String newLine, final Room room) {
        output.append(room.getId()).append(" ").append(room.getName()).append(" ").append(room.getItems().isEmpty() ? "None" : room.getItems()).append(newLine);
    }
}
