package mazemap.routing.output;

import java.util.List;
import java.util.Set;

import mazemap.data.core.Item;
import mazemap.data.core.Move;

import static mazemap.routing.output.RetroRoutePuzzleResultStatus.FAILURE;
import static mazemap.routing.output.RetroRoutePuzzleResultStatus.SUCCESS;

public class RetroRoutePuzzleResult {

    private final List<Move> route;
    private final Set<Item> itemsNotFound;

    public RetroRoutePuzzleResult(final List<Move> route,
                                  final Set<Item> itemsNotFound) {
        this.route = route;
        this.itemsNotFound = itemsNotFound;
    }

    public RetroRoutePuzzleResultStatus getStatus() {
        return itemsNotFound.isEmpty() ? SUCCESS : FAILURE;
    }

    public List<Move> getRoute() {
        return route;
    }

    public Set<Item> getItemsNotFound() {
        return itemsNotFound;
    }
}
