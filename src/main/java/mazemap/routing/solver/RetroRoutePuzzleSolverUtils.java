package mazemap.routing.solver;

import mazemap.data.core.Item;
import mazemap.data.core.Move;
import mazemap.data.core.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;

class RetroRoutePuzzleSolverUtils {

    static List<Move> computeMovesFromStartToEnd(final Integer start,
                                                 final Integer end,
                                                 final Map<Integer, Integer> roomOrigin) {
        final Integer target = roomOrigin.get(end);
        Integer initialPosition = start;
        final List<Move> moves = new ArrayList<>();
        while (!Objects.equals(initialPosition, target)) {
            final Integer current = initialPosition;
            initialPosition = roomOrigin.get(initialPosition);
            moves.add(new Move(current, initialPosition));
        }
        moves.add(new Move(target, end));
        return moves;
    }

    static void collectItems(final Room room, final Set<Item> itemsNotFound) {
        room.getItems().forEach(itemsNotFound::remove);
    }

    static boolean neighbourRoomCanBeExplored(final List<Room> neighbours,
                                              final Stack<Room> nextRoomsToExploreNeighbours,
                                              final List<Room> neighbourExploredRoom) {
        return neighbours.stream().anyMatch(neighbourRoom ->
                canExploreRoom(neighbourRoom, neighbourExploredRoom, nextRoomsToExploreNeighbours));
    }

    static boolean canExploreRoom(final Room room,
                                  final List<Room> neighbourExploredRoom,
                                  final Stack<Room> nextRoomsToExploreNeighbours) {
        return !neighbourExploredRoom.contains(room) && !nextRoomsToExploreNeighbours.contains(room);
    }
}
