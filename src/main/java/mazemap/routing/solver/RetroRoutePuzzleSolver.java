package mazemap.routing.solver;

import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import mazemap.data.core.Move;
import mazemap.data.core.Room;
import mazemap.data.core.input.RetroRoutePuzzleInput;
import mazemap.routing.output.RetroRoutePuzzleResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;

import static mazemap.routing.solver.RetroRoutePuzzleSolverUtils.canExploreRoom;
import static mazemap.routing.solver.RetroRoutePuzzleSolverUtils.collectItems;
import static mazemap.routing.solver.RetroRoutePuzzleSolverUtils.computeMovesFromStartToEnd;
import static mazemap.routing.solver.RetroRoutePuzzleSolverUtils.neighbourRoomCanBeExplored;

public class RetroRoutePuzzleSolver {

    private final MazeMap mazeMap;
    private final Integer startRoomId;
    private final Set<Item> itemsToCollect;

    public RetroRoutePuzzleSolver(final MazeMap mazeMap,
                                  final Integer startRoomId,
                                  final Set<Item> itemsToCollect) {
        this.mazeMap = mazeMap;
        this.startRoomId = startRoomId;
        this.itemsToCollect = itemsToCollect;
    }

    public RetroRoutePuzzleSolver(final RetroRoutePuzzleInput inputData) {
        this(inputData.getMazeMap(), inputData.getStartRoomId(), inputData.getItemsToCollect());
    }

    public RetroRoutePuzzleResult solve() {
        final List<Move> route = new ArrayList<>();
        final Set<Item> itemsNotFound = new HashSet<>(itemsToCollect);
        final Room startRoom = mazeMap.findRoomById(startRoomId);
        final Stack<Room> nextRoomsToExploreNeighbours = new Stack<>();
        final List<Room> neighbourExploredRoom = new ArrayList<>();
        //<key> room is reached by the <value> room
        final Map<Integer, Integer> roomOrigin = new HashMap<>();
        nextRoomsToExploreNeighbours.add(startRoom);
        Integer currentPosition = startRoomId;
        collectItems(startRoom, itemsNotFound);
        while (!nextRoomsToExploreNeighbours.isEmpty() && !itemsNotFound.isEmpty()) {
            //explore neighbours
            final Room currentRoom = nextRoomsToExploreNeighbours.pop();
            neighbourExploredRoom.add(currentRoom);
            //If no neighbour rooms can be explored, try with next (continue) in nextRoomsToExploreNeighbours stack
            if (!neighbourRoomCanBeExplored(currentRoom.getNeighbours(), nextRoomsToExploreNeighbours, neighbourExploredRoom)) {
                continue;
            }
            //A neighbour room can be explored, add the route to the current room if not already there
            if (!Objects.equals(currentPosition, currentRoom.getId())) {
                route.addAll(computeMovesFromStartToEnd(currentPosition, currentRoom.getId(), roomOrigin));
            }
            //Explore neighbour rooms, collect Items and update route
            for (final Room neighbour : currentRoom.getNeighbours()) {
                if (canExploreRoom(neighbour, neighbourExploredRoom, nextRoomsToExploreNeighbours)) {
                    nextRoomsToExploreNeighbours.add(neighbour);
                    roomOrigin.put(neighbour.getId(), currentRoom.getId());
                    route.add(new Move(currentRoom.getId(), neighbour.getId()));
                    collectItems(neighbour, itemsNotFound);
                    if (itemsNotFound.isEmpty())
                        break;
                }
            }
            //current position is the last neighbour room added in nextRoomsToExploreNeighbours
            currentPosition = nextRoomsToExploreNeighbours.peek().getId();
        }
        return new RetroRoutePuzzleResult(route, itemsNotFound);
    }
}
