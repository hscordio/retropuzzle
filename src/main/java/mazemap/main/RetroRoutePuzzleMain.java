package mazemap.main;

import mazemap.data.core.input.FileInfo;
import mazemap.data.core.input.ParameterLoader;
import mazemap.data.core.input.RetroRoutePuzzleInput;
import mazemap.data.core.input.RetroRoutePuzzleInputBuilder;
import mazemap.data.core.input.RetroRoutePuzzleParameters;
import mazemap.routing.output.RetroRoutePuzzleOutput;
import mazemap.routing.output.RetroRoutePuzzleResult;
import mazemap.routing.solver.RetroRoutePuzzleSolver;

import java.io.IOException;

import static mazemap.data.core.input.ArgumentParser.getFileInfo;

public class RetroRoutePuzzleMain {

    public static void main(final String[] args) {
        try {
            final RetroRoutePuzzleParameters parameters = getRetroRoutePuzzleParameters(getFileInfo(args));
            final RetroRoutePuzzleInput retroRoutePuzzleInput = buildRetroRoutePuzzleInput(parameters);
            System.out.println(retroRoutePuzzleInput);
            final RetroRoutePuzzleResult retroRoutePuzzleResult = solveRetroRoutePuzzle(retroRoutePuzzleInput);
            final RetroRoutePuzzleOutput output = new RetroRoutePuzzleOutput(retroRoutePuzzleResult, retroRoutePuzzleInput.getMazeMap());
            System.out.println(output);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private static RetroRoutePuzzleParameters getRetroRoutePuzzleParameters(final FileInfo fileInfo) throws IOException {
        final ParameterLoader parameterLoader = new ParameterLoader(fileInfo);
        return parameterLoader.load();
    }

    private static RetroRoutePuzzleInput buildRetroRoutePuzzleInput(final RetroRoutePuzzleParameters parameters) throws Exception {
        final RetroRoutePuzzleInputBuilder retroRoutePuzzleInputFactory = new RetroRoutePuzzleInputBuilder(parameters);
        return retroRoutePuzzleInputFactory.buildRetroRoutePuzzleInput();
    }

    private static RetroRoutePuzzleResult solveRetroRoutePuzzle(final RetroRoutePuzzleInput retroRoutePuzzleInput) {
        final RetroRoutePuzzleSolver solver = new RetroRoutePuzzleSolver(retroRoutePuzzleInput);
        return solver.solve();
    }
}
