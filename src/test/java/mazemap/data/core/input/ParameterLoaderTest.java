package mazemap.data.core.input;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PropertyLoader.class})
public class ParameterLoaderTest {

    @Mock
    FileInfo fileInfoMock;

    @InjectMocks
    private ParameterLoader underTest;

    private final Properties propertiesMock = mock(Properties.class);

    @Before
    public void setUp() throws Exception {
        mockStatic(PropertyLoader.class);
        when(PropertyLoader.loadPropertiesFromFile(fileInfoMock)).thenReturn(propertiesMock);
    }

    @Test
    public void shouldBuildRetroRoutePuzzleInput() throws Exception {
        when(fileInfoMock.getExternal()).thenReturn(false);
        when(propertiesMock.getProperty("maze.file")).thenReturn("/fake/maze/path");
        when(propertiesMock.getProperty("maze.start.room")).thenReturn("1");
        when(propertiesMock.getProperty("maze.items.collect")).thenReturn("item1, item2, item3");

        final RetroRoutePuzzleParameters retroRoutePuzzleParametersActual = underTest.load();

        final RetroRoutePuzzleParameters retroRoutePuzzleParametersExpected = new RetroRoutePuzzleParameters(
                new FileInfo(false, "/fake/maze/path"),
                "1",
                "item1, item2, item3"
        );
        assertEquals(retroRoutePuzzleParametersExpected,retroRoutePuzzleParametersActual);
    }
}