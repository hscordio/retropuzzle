package mazemap.data.core.input;

import com.google.common.collect.ImmutableSet;
import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MazeMapFactory.class})
public class RetroRoutePuzzleInputBuilderTest {

    @Mock
    RetroRoutePuzzleParameters retroRoutePuzzleParameters;
    @InjectMocks
    private RetroRoutePuzzleInputBuilder underTest;

    private final FileInfo fileInfo = new FileInfo(false, "/fake/internal/file");
    private final MazeMap mazeMapMock = mock(MazeMap.class);

    @Before
    public void setUp() throws Exception {
        mockStatic(MazeMapFactory.class);
        when(MazeMapFactory.buildMaze(fileInfo)).thenReturn(mazeMapMock);
    }

    @Test
    public void shouldBuildRetroRoutePuzzleInput() throws Exception {
        when(retroRoutePuzzleParameters.getMazeFile()).thenReturn(fileInfo);
        when(retroRoutePuzzleParameters.getItemsToCollect()).thenReturn("Potted Plant, Pillow, Knife");
        when(retroRoutePuzzleParameters.getStartRoomId()).thenReturn("1");

        final RetroRoutePuzzleInput retroRoutePuzzleInputActual = underTest.buildRetroRoutePuzzleInput();

        final ImmutableSet<Item> expectedItems = ImmutableSet.of(
                new Item("Knife"),
                new Item("Pillow"),
                new Item("Potted Plant"));
        final RetroRoutePuzzleInput retroRoutePuzzleInputExpected = new RetroRoutePuzzleInput(
                mazeMapMock, 1, expectedItems, "/fake/internal/file");
        assertEquals(retroRoutePuzzleInputExpected, retroRoutePuzzleInputActual);
        verifyStatic(MazeMapFactory.class);
        MazeMapFactory.buildMaze(any(FileInfo.class));
    }
}