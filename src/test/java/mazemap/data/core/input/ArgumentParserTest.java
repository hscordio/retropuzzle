package mazemap.data.core.input;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArgumentParserTest {

    @Test
    public void shouldReturnDefaultInternalFilePathWhenArgumentIsNull() {
        final String [] args = null;

        final FileInfo fileInfo = ArgumentParser.getFileInfo(args);

        Assert.assertEquals("/parameters/parameters-test.properties", fileInfo.getPath());
        Assert.assertFalse(fileInfo.getExternal());
    }

    @Test
    public void shouldReturnDefaultInternalFilePathWhenArgumentIsAnEmptyArray() {
        final String [] args = {};

        final FileInfo fileInfo = ArgumentParser.getFileInfo(args);

        Assert.assertEquals("/parameters/parameters-test.properties", fileInfo.getPath());
        Assert.assertFalse(fileInfo.getExternal());
    }

    @Test
    public void shouldReturnSpecifiedInternalFilePathWhenArgumentHasOneParameter() {
        final String [] args = {"/internal/file/path"};

        final FileInfo fileInfo = ArgumentParser.getFileInfo(args);

        Assert.assertEquals("/internal/file/path", fileInfo.getPath());
        Assert.assertFalse(fileInfo.getExternal());
    }

    @Test
    public void shouldReturnSpecifiedExternalFilePathWhenArgumentHasOneParameter() {
        final String[] args = {"e", "/external/file/path"};

        final FileInfo fileInfo = ArgumentParser.getFileInfo(args);

        Assert.assertEquals("/external/file/path", fileInfo.getPath());
        Assert.assertTrue(fileInfo.getExternal());
    }
}