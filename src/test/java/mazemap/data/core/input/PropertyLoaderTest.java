package mazemap.data.core.input;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.InputStream;
import java.util.Properties;

import static mazemap.data.core.input.FileRetriever.retrieveFile;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PropertyLoader.class, FileRetriever.class})
public class PropertyLoaderTest {

    private final FileInfo fileInfo = new FileInfo(false, "/fake/internal/file");
    private final InputStream inputStreamMock = mock(InputStream.class);
    private final Properties propertiesMock = mock(Properties.class);

    @Before
    public void setUp() throws Exception {
        mockStatic(FileRetriever.class);
        when(retrieveFile(fileInfo)).thenReturn(inputStreamMock);
        whenNew(Properties.class).withNoArguments().thenReturn(propertiesMock);
        doNothing().when(propertiesMock).load(inputStreamMock);
        doNothing().when(inputStreamMock).close();
    }

    @Test
    public void shouldLoadPropertiesFromFile() throws Exception {
        final Properties properties = PropertyLoader.loadPropertiesFromFile(fileInfo);

        assertEquals(propertiesMock, properties);
        verify(propertiesMock).load(inputStreamMock);
        verify(inputStreamMock).close();
        verifyStatic(FileRetriever.class);
        FileRetriever.retrieveFile(any(FileInfo.class));
    }
}