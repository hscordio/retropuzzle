package mazemap.routing.solver;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import mazemap.data.core.Item;
import mazemap.data.core.Move;
import mazemap.data.core.Room;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class RetroRoutePuzzleSolverUtilsTest {

    @Test
    public void shouldComputeMovesFromStartToEnd() {
        final ImmutableMap<Integer, Integer> roomOrigin = ImmutableMap.<Integer, Integer>builder()
                .put(2, 1)
                .put(4, 7)
                .put(5, 6)
                .put(6, 4)
                .put(7, 1)
                .put(8, 6)
                .put(9, 8)
                .put(10, 7)
                .put(11, 7)
                .build();

        final List<Move> moves = RetroRoutePuzzleSolverUtils.computeMovesFromStartToEnd(9, 2, roomOrigin);
        final List<Move> expectedPath = asList(
                new Move(9, 8),
                new Move(8, 6),
                new Move(6, 4),
                new Move(4, 7),
                new Move(7, 1),
                new Move(1, 2));

        assertEquals(expectedPath, moves);
    }


    @Test
    public void shouldCollectItems() {
        final Room room = new Room(
                1,
                "TestRoom",
                null,
                null,
                null,
                null,
                ImmutableSet.of(new Item("Knife")));

        final HashSet<Item> items = Sets.newHashSet(new Item("Knife"), new Item("Pillow"));
        RetroRoutePuzzleSolverUtils.collectItems(room, items);

        assertEquals(ImmutableSet.of(new Item("Pillow")), items);
    }

    @Test
    public void shouldCollectMoreItems() {
        final Room room = new Room(
                1,
                "TestRoom",
                null,
                null,
                null,
                null,
                ImmutableSet.of(new Item("Knife"), new Item("Pillow")));

        final HashSet<Item> items = Sets.newHashSet(
                new Item("Knife"),
                new Item("Pillow"),
                new Item("Potted Plant"));
        RetroRoutePuzzleSolverUtils.collectItems(room, items);

        assertEquals(ImmutableSet.of(new Item("Potted Plant")), items);
    }

    @Test
    public void shouldNotCollectItemsWhenItemIsNotInTheRoom() {
        final Room room = new Room(
                1,
                "TestRoom",
                null,
                null,
                null,
                null,
                ImmutableSet.of(new Item("Potted Plant")));

        final HashSet<Item> items = Sets.newHashSet(new Item("Knife"), new Item("Pillow"));
        RetroRoutePuzzleSolverUtils.collectItems(room, items);

        assertEquals(ImmutableSet.of(new Item("Knife"), new Item("Pillow")), items);
    }

    @Test
    public void shouldReturnTrueWhenANeighbourRoomCanBeExplored() {
        final Room room1 = createRoomByIdAndName(1,"TestRoom1");
        final Room room7 = createRoomByIdAndName(7,"TestRoom7");
        final List<Room> neighbours = asList(room1, room7);

        final Room room4 = createRoomByIdAndName(4,"TestRoom4");
        final Room room3 = createRoomByIdAndName(3,"TestRoom3");
        final Stack<Room> nextRoomsToExploreNeighbours = new Stack<>();
        nextRoomsToExploreNeighbours.push(room4);
        nextRoomsToExploreNeighbours.push(room3);
        nextRoomsToExploreNeighbours.push(room1);

        final Room room8 = createRoomByIdAndName(8,"TestRoom8");
        final Room room9 = createRoomByIdAndName(9,"TestRoom9");
        final List<Room> neighbourExploredRoom = asList(room8, room9);

        final boolean neighbourRoomCanBeExplored = RetroRoutePuzzleSolverUtils.neighbourRoomCanBeExplored(
                neighbours,
                nextRoomsToExploreNeighbours,
                neighbourExploredRoom);

        assertTrue(neighbourRoomCanBeExplored);
    }

    @Test
    public void shouldReturnFalseWhenANeighbourRoomCanNotBeExplored() {
        final Room room1 = createRoomByIdAndName(1,"TestRoom1");
        final Room room7 = createRoomByIdAndName(7,"TestRoom7");
        final List<Room> neighbours = asList(room1, room7);

        final Room room4 = createRoomByIdAndName(4,"TestRoom4");
        final Room room3 = createRoomByIdAndName(3,"TestRoom3");

        final Stack<Room> nextRoomsToExploreNeighbours = new Stack<>();
        nextRoomsToExploreNeighbours.push(room4);
        nextRoomsToExploreNeighbours.push(room3);
        nextRoomsToExploreNeighbours.push(room1);

        final Room room8 = createRoomByIdAndName(8,"TestRoom8");
        final Room room9 = createRoomByIdAndName(9,"TestRoom9");
        final List<Room> neighbourExploredRoom = asList(room7, room8, room9);

        final boolean neighbourRoomCanBeExplored = RetroRoutePuzzleSolverUtils.neighbourRoomCanBeExplored(
                neighbours,
                nextRoomsToExploreNeighbours,
                neighbourExploredRoom);

        assertFalse(neighbourRoomCanBeExplored);
    }

    private Room createRoomByIdAndName(int id, String name) {
        return new Room(
                id,
                name,
                null,
                null,
                null,
                null,
                null);
    }

}