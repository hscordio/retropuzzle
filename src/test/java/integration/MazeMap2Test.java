package integration;

import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import mazemap.data.core.Move;
import mazemap.data.core.Room;
import mazemap.data.core.input.ParameterLoader;
import mazemap.data.core.input.RetroRoutePuzzleInput;
import mazemap.data.core.input.RetroRoutePuzzleInputBuilder;
import mazemap.data.core.input.RetroRoutePuzzleParameters;
import mazemap.routing.output.RetroRoutePuzzleResult;
import mazemap.routing.output.RetroRoutePuzzleResultStatus;
import mazemap.routing.solver.RetroRoutePuzzleSolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static mazemap.data.core.input.ArgumentParser.getFileInfo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MazeMap2Test {

    private MazeMap mazeMap = null;
    private Integer startRoomId = null;
    private Set<Item> itemsToCollect = null;
    private RetroRoutePuzzleParameters parameters = null;

    @Before
    public void setUp() throws Exception {
        final String[] args = {"/parameters/parameters-test2.properties"};
        final ParameterLoader parameterLoader = new ParameterLoader(getFileInfo(args));
        parameters = parameterLoader.load();
        final RetroRoutePuzzleInputBuilder retroRoutePuzzleInputFactory = new RetroRoutePuzzleInputBuilder(parameters);
        final RetroRoutePuzzleInput retroRoutePuzzleInput = retroRoutePuzzleInputFactory.buildRetroRoutePuzzleInput();
        mazeMap = retroRoutePuzzleInput.getMazeMap();
        startRoomId = retroRoutePuzzleInput.getStartRoomId();
        itemsToCollect = retroRoutePuzzleInput.getItemsToCollect();
    }

    @Test
    public void testParameterLoader() {
        assertEquals("4", parameters.getStartRoomId());
        assertEquals("/maps/map2.json", parameters.getMazeFile().getPath());
        assertEquals("Potted Plant, Pillow, Knife", parameters.getItemsToCollect());
    }

    @Test
    public void testStartingRoomIsCorrect() {
        assertEquals(Integer.valueOf(4), startRoomId);
    }

    @Test
    public void testItemToCollectIsNotEmpty() {
        assertTrue(itemsToCollect != null && !itemsToCollect.isEmpty());
    }

    @Test
    public void testItemToCollectIsCorrect() {
        assertEquals(3, itemsToCollect.size());
        assertTrue(itemsToCollect.contains(new Item("Potted Plant")));
        assertTrue(itemsToCollect.contains(new Item("Pillow")));
        assertTrue(itemsToCollect.contains(new Item("Knife")));
    }

    @Test
    public void testBuildMazeAllRooms() {
        final List<Integer> builtRoomIds = mazeMap.getRooms().stream().map(Room::getId).collect(Collectors.toList());
        final List<Integer> expectedRoomIds = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        assertEquals(expectedRoomIds, builtRoomIds);
    }

    @Test
    public void testFindRoomById() {
        final Room startRoom = mazeMap.findRoomById(startRoomId);
        Assert.assertNotNull(startRoom);
        assertEquals(Integer.valueOf(4), startRoom.getId());
        assertEquals(Integer.valueOf(6), startRoom.getNorth());
    }

    @Test
    public void testNeighbours() {
        final Room room4 = mazeMap.findRoomById(4);
        final Room room7 = mazeMap.findRoomById(7);
        assertTrue(room4.getNeighbours().contains(room7));
        assertTrue(room7.getNeighbours().contains(room4));
    }

    @Test
    public void testMazeSolve() {
        final RetroRoutePuzzleSolver solver = new RetroRoutePuzzleSolver(mazeMap, startRoomId, itemsToCollect);
        final RetroRoutePuzzleResult result = solver.solve();
        assertEquals(result.getStatus(), RetroRoutePuzzleResultStatus.SUCCESS);
        assertTrue(result.getItemsNotFound().isEmpty());
        assertTrue(result.getRoute().contains(new Move(4, 2)));
        assertTrue(result.getRoute().contains(new Move(4, 6)));
        assertTrue(result.getRoute().contains(new Move(2, 3)));
        assertFalse(result.getRoute().contains(new Move(5, 2)));
        assertFalse(result.getRoute().contains(new Move(5, 6)));
    }
}
