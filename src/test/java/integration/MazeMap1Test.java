package integration;

import mazemap.data.core.Item;
import mazemap.data.core.MazeMap;
import mazemap.data.core.Move;
import mazemap.data.core.Room;
import mazemap.data.core.input.ArgumentParser;
import mazemap.data.core.input.ParameterLoader;
import mazemap.data.core.input.RetroRoutePuzzleInput;
import mazemap.data.core.input.RetroRoutePuzzleInputBuilder;
import mazemap.data.core.input.RetroRoutePuzzleParameters;
import mazemap.routing.output.RetroRoutePuzzleResult;
import mazemap.routing.output.RetroRoutePuzzleResultStatus;
import mazemap.routing.solver.RetroRoutePuzzleSolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MazeMap1Test {

    private MazeMap mazeMap = null;
    private Integer startRoomId = null;
    private Set<Item> itemsToCollect = null;
    private RetroRoutePuzzleParameters parameters = null;

    @Before
    public void setUp() throws Exception {
        final String[] args = {"/parameters/parameters-test1.properties"};
        final ParameterLoader parameterLoader = new ParameterLoader(ArgumentParser.getFileInfo(args));
        parameters = parameterLoader.load();
        final RetroRoutePuzzleInputBuilder retroRoutePuzzleInputFactory = new RetroRoutePuzzleInputBuilder(parameters);
        final RetroRoutePuzzleInput retroRoutePuzzleInput = retroRoutePuzzleInputFactory.buildRetroRoutePuzzleInput();
        mazeMap = retroRoutePuzzleInput.getMazeMap();
        startRoomId = retroRoutePuzzleInput.getStartRoomId();
        itemsToCollect = retroRoutePuzzleInput.getItemsToCollect();
    }

    @Test
    public void testParameterLoader() {
        assertEquals("2", parameters.getStartRoomId());
        assertEquals("/maps/map1.json", parameters.getMazeFile().getPath());
        assertEquals("Knife, Potted Plant", parameters.getItemsToCollect());
    }

    @Test
    public void testStartingRoomIsCorrect() {
        assertEquals(Integer.valueOf(2), startRoomId);
    }

    @Test
    public void testItemToCollectIsNotEmpty() {
        assertTrue(itemsToCollect != null && !itemsToCollect.isEmpty());
    }

    @Test
    public void testItemToCollectIsCorrect() {
        assertEquals(2, itemsToCollect.size());
        assertTrue(itemsToCollect.contains(new Item("Potted Plant")));
        assertTrue(itemsToCollect.contains(new Item("Knife")));
        assertFalse(itemsToCollect.contains(new Item("Pillow")));
    }

    @Test
    public void testBuildMazeAllRooms() {
        final List<Integer> builtRoomIds = mazeMap.getRooms().stream().map(Room::getId).collect(Collectors.toList());
        final List<Integer> expectedRoomIds = asList(1, 2, 3, 4);
        assertEquals(expectedRoomIds, builtRoomIds);
    }

    @Test
    public void testFindRoomById() {
        final Room startRoom = mazeMap.findRoomById(startRoomId);
        Assert.assertNotNull(startRoom);
        assertEquals(Integer.valueOf(2), startRoom.getId());
        assertEquals(Integer.valueOf(1), startRoom.getSouth());
    }

    @Test
    public void testNeighbours() {
        final Room room2 = mazeMap.findRoomById(2);
        final Room room4 = mazeMap.findRoomById(4);
        assertTrue(room2.getNeighbours().contains(room4));
        assertTrue(room4.getNeighbours().contains(room2));
    }

    @Test
    public void testMazeSolve() {
        final RetroRoutePuzzleSolver solver = new RetroRoutePuzzleSolver(mazeMap, startRoomId, itemsToCollect);
        final RetroRoutePuzzleResult result = solver.solve();
        assertEquals(result.getStatus(), RetroRoutePuzzleResultStatus.SUCCESS);
        assertTrue(result.getItemsNotFound().isEmpty());
        assertTrue(result.getRoute().contains(new Move(2, 4)));
        assertTrue(result.getRoute().contains(new Move(2, 3)));
        assertFalse(result.getRoute().contains(new Move(3, 2)));
    }
}
